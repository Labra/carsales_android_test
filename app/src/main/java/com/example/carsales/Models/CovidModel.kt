package com.example.carsales.Models
import com.google.gson.annotations.SerializedName

data class CovidModel (
    @SerializedName("data") val data : Data
)
data class Data(
    @SerializedName("date") val date : String,
    @SerializedName("last_update") val last_update : String,
    @SerializedName("confirmed") val confirmed : Int,
    @SerializedName("deaths") val deaths : Int,
    @SerializedName("confirmed_diff") val confirmed_diff : Int,
    @SerializedName("deaths_diff") val deaths_diff : Int,
    @SerializedName("recovered") val recovered : Int,
    @SerializedName("recovered_diff") val recovered_diff : Int,
    @SerializedName("active") val active : Int,
    @SerializedName("active_diff") val active_diff : Int,
    @SerializedName("fatality_rate") val fatality_rate : Float
)