package com.example.carsales.Managers
import com.example.carsales.Interfaces.GetDailyReport
import com.example.carsales.Models.CovidModel
import retrofit2.*
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ApiManager {
    object SharedInstance {

        private val call: Retrofit = Retrofit.Builder()
            .baseUrl("https://covid-19-statistics.p.rapidapi.com/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        private const val apiKey: String = "96afa298cbmsh913f910f914494cp110c39jsn01a32d68445e"

        public fun getCovidData(selectedDate: String,  listener: GetCovidListener) {
            call.create(GetDailyReport::class.java).listRepos(apiKey, selectedDate).enqueue(object : Callback<CovidModel?> {
                override fun onResponse(call: Call<CovidModel?>, response: Response<CovidModel?>) {
                    listener.onSuceess(response.body())
                }
                override fun onFailure(call: Call<CovidModel?>, t: Throwable) {
                    listener.onError(t)
                }
            });
        }
    }
}

interface GetCovidListener {
    fun onSuceess(covidModel: CovidModel?)
    fun onError(error: Throwable)
}