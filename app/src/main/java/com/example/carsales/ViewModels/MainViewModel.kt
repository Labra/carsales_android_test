package com.example.carsales.ViewModels
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.carsales.Managers.ApiManager
import com.example.carsales.Managers.GetCovidListener
import com.example.carsales.Models.CovidModel

class MainViewModel: ViewModel() {

    val covidLiveData: MutableLiveData<CovidModel> = MutableLiveData()
    val loaderLiveData: MutableLiveData<Int> = MutableLiveData()

    fun getCovid(currentDate: String){
        ApiManager.SharedInstance.getCovidData(currentDate, object : GetCovidListener {
            override fun onSuceess(covidModel: CovidModel?) {
                covidLiveData.postValue(covidModel)
                loaderLiveData.postValue(View.GONE)
            }
            override fun onError(error: Throwable) {
                covidLiveData.postValue(null)
                loaderLiveData.postValue(View.GONE)
            }
        })
    }
}