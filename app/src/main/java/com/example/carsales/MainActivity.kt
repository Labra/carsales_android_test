package com.example.carsales
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.example.carsales.Models.CovidModel
import com.example.carsales.ViewModels.MainViewModel
import com.example.carsales.databinding.ActivityMainBinding
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : FragmentActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var mainViewModel: MainViewModel
    var currentDateName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        mainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        mainViewModel.covidLiveData.observe(this, androidx.lifecycle.Observer { covidModel ->
            setCovidLabelData(covidModel)
        })
        mainViewModel.loaderLiveData.observe(this,{
            binding.loaderView.loaderLayout.visibility = it
        })

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH-1)

        val month_date = SimpleDateFormat("d MMMM yyyy")
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        currentDateName = month_date.format(calendar.getTime())
        binding.textView.text = currentDateName
        mainViewModel.loaderLiveData.postValue(View.VISIBLE)
        mainViewModel.getCovid(formatCurrentDate(year,month,day))

        binding.button.setOnClickListener {
            mainViewModel.loaderLiveData.postValue(View.VISIBLE)
            val datepicker = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                var currentDate = formatCurrentDate(year,monthOfYear,dayOfMonth)
                mainViewModel.getCovid(currentDate)
            }, year, month, day)
            datepicker.show()
        }
    }

    fun setCovidLabelData(covidObj: CovidModel?){
        mainViewModel.loaderLiveData.postValue(View.GONE)
        covidObj?.let {
            binding.textView2.text = getString(R.string.covid_confirmed)+covidObj.data.confirmed.toString()
            binding.textView3.text = getString(R.string.covid_deaths)+covidObj.data.deaths.toString()
        }?: run {
            binding.textView2.text = ""
            binding.textView3.text = ""
        }
    }

    fun formatCurrentDate(year: Int, month: Int, day: Int):String{
        var fixed_month = ""
        var fixed_day = ""
        if(month < 10 ){
            fixed_month = "0"+(month+1).toString()
        }else{
            fixed_month = (month+1).toString()
        }
        if (day < 10 ){
            fixed_day = "0"+(day).toString()
        }else{
            fixed_day = (day).toString()
        }
        val currente_date = year.toString()+"-"+fixed_month+"-"+fixed_day
        return currente_date
    }
}