package com.example.carsales.Interfaces
import com.example.carsales.Models.CovidModel
import retrofit2.Call
import retrofit2.http.Query
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers

interface GetDailyReport {
    @Headers("Content-Type: application/json")
    @GET("reports/total")
    fun listRepos(@Header ("X-RapidAPI-Key") api_key: String, @Query("date") current_date: String): Call<CovidModel>
}